﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Newtonsoft.Json;
using System.Threading;

namespace WoT_Maps_Stat
{
    public partial class Form1 : Form
    {

        public string v = "{\"china\":{\"Ch01_Type59\":8,\"Ch02_Type62\":7,\"Ch03_WZ-111\":9,\"Ch04_T34_1\":7,\"Ch05_T34_2\":8,\"Ch06_Renault_NC31\":1,\"Ch07_Vickers_MkE_Type_BT26\":2,\"Ch08_Type97_Chi_Ha\":3,\"Ch09_M5\":4,\"Ch10_IS2\":7,\"Ch11_110\":8,\"Ch12_111_1_2_3\":9,\"Ch15_59_16\":6,\"Ch16_WZ_131\":7,\"Ch17_WZ131_1_WZ132\":8,\"Ch18_WZ-120\":9,\"Ch19_121\":10,\"Ch20_Type58\":6,\"Ch21_T34\":5,\"Ch22_113\":10,\"Ch23_112\":8},\"france\":{\"AMX38\":3,\"AMX40\":4,\"AMX50_Foch\":9,\"AMX_105AM\":5,\"AMX_12t\":6,\"AMX_13F3AM\":6,\"AMX_13_75\":7,\"AMX_13_90\":8,\"AMX_50Fosh_155\":10,\"AMX_50_100\":8,\"AMX_50_120\":9,\"AMX_AC_Mle1946\":7,\"AMX_AC_Mle1948\":8,\"AMX_M4_1945\":7,\"AMX_Ob_Am105\":4,\"ARL_44\":6,\"ARL_V39\":6,\"B1\":4,\"Bat_Chatillon155\":10,\"Bat_Chatillon155_55\":9,\"Bat_Chatillon155_58\":10,\"Bat_Chatillon25t\":10,\"BDR_G1B\":5,\"D1\":2,\"D2\":3,\"ELC_AMX\":5,\"F10_AMX_50B\":10,\"FCM_36Pak40\":3,\"FCM_50t\":8,\"Hotchkiss_H35\":2,\"Lorraine155_50\":7,\"Lorraine155_51\":8,\"Lorraine39_L_AM\":3,\"Lorraine40t\":9,\"RenaultBS\":2,\"RenaultFT\":1,\"RenaultFT_AC\":2,\"RenaultUE57\":3,\"Somua_Sau_40\":4,\"S_35CA\":5,\"_105_leFH18B2\":5},\"germany\":{\"Auf_Panther\":7,\"B-1bis_captured\":4,\"Bison_I\":3,\"DickerMax\":6,\"E-100\":10,\"E-25\":7,\"E-50\":9,\"E-75\":9,\"E50_Ausf_M\":10,\"Ferdinand\":8,\"G20_Marder_II\":3,\"Grille\":5,\"GW_Mk_VIe\":2,\"GW_Tiger_P\":7,\"G_E\":10,\"G_Panther\":7,\"G_Tiger\":8,\"H39_captured\":2,\"Hetzer\":4,\"Hummel\":6,\"Indien_Panzer\":8,\"JagdPanther\":7,\"JagdPantherII\":8,\"JagdPzIV\":6,\"JagdPz_E100\":10,\"JagdTiger\":9,\"JagdTiger_SdKfz_185\":8,\"Leopard1\":10,\"Lowe\":8,\"Ltraktor\":1,\"Maus\":10,\"Panther_II\":8,\"Panther_M10\":7,\"PanzerJager_I\":2,\"Pro_Ag_A\":9,\"Pz35t\":2,\"Pz38t\":3,\"Pz38_NA\":4,\"PzI\":2,\"PzII\":2,\"PzIII\":4,\"PzIII_A\":3,\"PzIII_IV\":5,\"PzIII_training\":5,\"PzII_J\":3,\"PzII_Luchs\":4,\"PzIV\":5,\"PzIV_Hydro\":5,\"PzIV_schmalturm\":6,\"PzI_ausf_C\":3,\"PzV\":7,\"PzVI\":7,\"PzVIB_Tiger_II\":8,\"PzVIB_Tiger_II_training\":8,\"PzVI_Tiger_P\":7,\"PzV_PzIV\":6,\"PzV_PzIV_ausf_Alfa\":6,\"PzV_training\":7,\"Pz_II_AusfG\":3,\"Pz_IV_AusfGH\":3,\"Pz_Sfl_IVb\":4,\"S35_captured\":3,\"StugIII\":5,\"Sturmpanzer_II\":4,\"T-15\":3,\"T-25\":5,\"VK1602\":5,\"VK2001DB\":4,\"VK2801\":6,\"VK3001H\":6,\"VK3001P\":6,\"VK3002DB\":7,\"VK3002DB_V1\":7,\"VK3601H\":6,\"VK4502A\":8,\"VK4502P\":9,\"VK7201\":0,\"Wespe\":3},\"uk\":{\"GB01_Medium_Mark_I\":1,\"GB03_Cruiser_Mk_I\":2,\"GB04_Valentine\":4,\"GB05_Vickers_Medium_Mk_II\":2,\"GB06_Vickers_Medium_Mk_III\":3,\"GB07_Matilda\":4,\"GB08_Churchill_I\":5,\"GB09_Churchill_VII\":6,\"GB10_Black_Prince\":7,\"GB11_Caernarvon\":8,\"GB12_Conqueror\":9,\"GB13_FV215b\":10,\"GB20_Crusader\":5,\"GB21_Cromwell\":6,\"GB22_Comet\":7,\"GB23_Centurion\":8,\"GB24_Centurion_Mk3\":9,\"GB32_Tortoise\":9,\"GB39_Universal_CarrierQF2\":2,\"GB40_Gun_Carrier_Churchill\":6,\"GB42_Valentine_AT\":3,\"GB48_FV215b_183\":10,\"GB51_Excelsior\":5,\"GB57_Alecto\":4,\"GB58_Cruiser_Mk_III\":2,\"GB59_Cruiser_Mk_IV\":3,\"GB60_Covenanter\":4,\"GB63_TOG_II\":6,\"GB68_Matilda_Black_Prince\":5,\"GB69_Cruiser_Mk_II\":3,\"GB70_FV4202_105\":10,\"GB71_AT_15A\":7,\"GB72_AT15\":8,\"GB73_AT2\":5,\"GB74_AT8\":6,\"GB75_AT7\":7,\"GB78_Sexton_I\":0},\"usa\":{\"A74_T1_E6\":2,\"M103\":9,\"M10_Wolverine\":5,\"M12\":7,\"M18_Hellcat\":6,\"M22_Locust\":3,\"M24_Chaffee\":5,\"M2_lt\":2,\"M2_med\":3,\"M36_Slagger\":6,\"M37\":4,\"M3_Grant\":4,\"M3_Stuart\":3,\"M40M43\":8,\"M41\":5,\"M44\":6,\"M46_Patton\":9,\"M48A1\":10,\"M4A2E4\":5,\"M4A3E8_Sherman\":6,\"M4_Sherman\":5,\"M53_55\":9,\"M5_Stuart\":4,\"M6\":6,\"M60\":0,\"M6A2E1\":8,\"M7_med\":5,\"M7_Priest\":3,\"M8A1\":4,\"MTLS-1G14\":3,\"Pershing\":8,\"Ram-II\":5,\"Sherman_Jumbo\":6,\"T110\":10,\"T110E3\":10,\"T110E4\":10,\"T14\":5,\"T18\":2,\"T1_Cunningham\":1,\"T1_E6\":2,\"T1_hvy\":5,\"T20\":7,\"T21\":6,\"T25_2\":7,\"T25_AT\":7,\"T26_E4_SuperPershing\":8,\"T28\":8,\"T28_Prototype\":8,\"T29\":7,\"T2_lt\":2,\"T2_med\":3,\"T30\":9,\"T32\":8,\"T34_hvy\":8,\"T40\":4,\"T49\":5,\"T54E1\":9,\"T57\":2,\"T57_58\":10,\"T69\":8,\"T71\":7,\"T82\":3,\"T92\":10,\"T95\":9},\"ussr\":{\"62A\":10,\"A-20\":4,\"A-32\":4,\"AT-1\":2,\"BT-2\":2,\"BT-7\":3,\"BT-SV\":3,\"Churchill_LL\":5,\"GAZ-74b\":4,\"IS-3\":8,\"IS-4\":10,\"IS-7\":10,\"IS\":7,\"IS8\":9,\"ISU-152\":8,\"KV-13\":7,\"KV-1s\":6,\"KV-220\":5,\"KV-220_action\":5,\"KV-3\":7,\"KV-5\":8,\"KV\":5,\"KV1\":5,\"KV2\":6,\"KV4\":8,\"M3_Stuart_LL\":3,\"Matilda_II_LL\":5,\"MS-1\":1,\"Object252\":8,\"Object263\":10,\"Object268\":10,\"Object_212\":0,\"Object_261\":10,\"Object_704\":9,\"Object_907\":0,\"Observer\":0,\"R80_KV1\":5,\"S-51\":7,\"ST_I\":9,\"SU-100\":6,\"SU-101\":8,\"SU-14\":8,\"SU-152\":7,\"SU-18\":2,\"SU-26\":3,\"SU-5\":4,\"SU-76\":3,\"SU-8\":6,\"SU-85\":5,\"SU100M1\":7,\"SU100Y\":6,\"SU101\":8,\"SU122A\":5,\"SU122_44\":8,\"SU122_54\":9,\"SU14_1\":7,\"SU_85I\":5,\"T-127\":3,\"T-26\":2,\"T-28\":4,\"T-34-85\":6,\"T-34\":5,\"T-43\":7,\"T-44\":8,\"T-46\":3,\"T-50\":4,\"T-54\":9,\"T-60\":2,\"T-70\":3,\"T150\":6,\"T62A\":10,\"T80\":4,\"Tetrarch_LL\":2,\"T_50_2\":5,\"Valentine_LL\":4}}";

        static string maps = "{\"westfeld\":{\"name\":\"Вестфилд\",\"b\":0,\"w\":0,\"k\":0},\"asia_miao\":{\"name\":\"Жемчужная река\",\"b\":0,\"w\":0,\"k\":0},\"karelia\":{\"name\":\"Карелия\",\"b\":0,\"w\":0,\"k\":0},\"komarin\":{\"name\":\"Комарин\",\"b\":0,\"w\":0,\"k\":0},\"north_america\":{\"name\":\"Лайв Окс\",\"b\":0,\"w\":0,\"k\":0},\"lakeville\":{\"name\":\"Лассвиль\",\"b\":0,\"w\":0,\"k\":0},\"siegfried_line\":{\"name\":\"Линия Зигфрида\",\"b\":0,\"w\":0,\"k\":0},\"malinovka\":{\"name\":\"Малиновка\",\"b\":0,\"w\":0,\"k\":0},\"monastery\":{\"name\":\"Монастырь\",\"b\":0,\"w\":0,\"k\":0},\"murovanka\":{\"name\":\"Мурованка\",\"b\":0,\"w\":0,\"k\":0},\"caucasus\":{\"name\":\"Перевал\",\"b\":0,\"w\":0,\"k\":0},\"port\":{\"name\":\"Порт\",\"b\":0,\"w\":0,\"k\":0},\"campania\":{\"name\":\"Провинция\",\"b\":0,\"w\":0,\"k\":0},\"prohorovka\":{\"name\":\"Прохоровка\",\"b\":0,\"w\":0,\"k\":0},\"redshire\":{\"name\":\"Редшир\",\"b\":0,\"w\":0,\"k\":0},\"hills\":{\"name\":\"Рудники\",\"b\":0,\"w\":0,\"k\":0},\"ruinberg\":{\"name\":\"Руинберг\",\"b\":0,\"w\":0,\"k\":0},\"fishing_bay\":{\"name\":\"Рыбацкая бухта\",\"b\":0,\"w\":0,\"k\":0},\"steppes\":{\"name\":\"Степи\",\"b\":0,\"w\":0,\"k\":0},\"canada_a\":{\"name\":\"Тихий берег\",\"b\":0,\"w\":0,\"k\":0},\"swamp\":{\"name\":\"Топь\",\"b\":0,\"w\":0,\"k\":0},\"munchen\":{\"name\":\"Уайдпарк\",\"b\":0,\"w\":0,\"k\":0},\"cliff\":{\"name\":\"Утес\",\"b\":0,\"w\":0,\"k\":0},\"fjord\":{\"name\":\"Фьорды\",\"b\":0,\"w\":0,\"k\":0},\"highway\":{\"name\":\"Хайвей\",\"b\":0,\"w\":0,\"k\":0},\"himmelsdorf\":{\"name\":\"Химмельсдорф\",\"b\":0,\"w\":0,\"k\":0},\"asia\":{\"name\":\"Хребет дракона\",\"b\":0,\"w\":0,\"k\":0},\"ensk\":{\"name\":\"Энск\",\"b\":0,\"w\":0,\"k\":0},\"crimea\":{\"name\":\"Южный берег\",\"b\":0,\"w\":0,\"k\":0},\"mannerheim_line\":{\"name\":\"Заполярье\",\"b\":0,\"w\":0,\"k\":0},\"asia_korea\":{\"name\":\"Священная долина\",\"b\":0,\"w\":0,\"k\":0},\"erlenberg\":{\"name\":\"Эрленберг\",\"b\":0,\"w\":0,\"k\":0},\"airfield\":{\"name\":\"Аэродром\",\"b\":0,\"w\":0,\"k\":0},\"desert\":{\"name\":\"Песчаная река\",\"b\":0,\"w\":0,\"k\":0},\"el_hallouf\":{\"name\":\"Эль-Халлуф\",\"b\":0,\"w\":0,\"k\":0}}";
        public Newtonsoft.Json.Linq.JObject oMaps = Newtonsoft.Json.Linq.JObject.Parse(maps);

        public string dir_path;

        public Form1()
        {
            InitializeComponent();
            foreach (var map in oMaps)
            {
                chart1.Series[0].Points.AddXY(map.Value["name"].ToString(), 0);
                chart2.Series[0].Points.AddXY(map.Value["name"].ToString(), 0);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog sel_dir = new System.Windows.Forms.FolderBrowserDialog();
            sel_dir.Description = "Выберите папку с реплееями";
            DialogResult dialogresult = sel_dir.ShowDialog();
            if (dialogresult == DialogResult.OK)
            {
                dir_path = sel_dir.SelectedPath;
            }
            sel_dir.Dispose();
            worker.RunWorkerAsync();
        }

        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            if (dir_path != null)
            {
                button1.Invoke((MethodInvoker)delegate
                {
                    button1.Enabled = false;
                });
                string[] files = Directory.GetFiles(@dir_path, "*.wotreplay");
                int files_count = files.Length;
                if (files_count > 0)
                {
                    int i = 0;
                    label2.Invoke((MethodInvoker)delegate
                    {
                        label2.Text = "0/" + files.Length.ToString();
                    });
                    foreach (string file in files)
                    {
                        if (worker.CancellationPending == false)
                        {
                            StreamReader rdr = new StreamReader(file, Encoding.ASCII);
                            string text = rdr.ReadToEnd();
                            rdr.Close();
                            string first = text.Substring(text.IndexOf("{\""), text.IndexOf("\"}") - text.IndexOf("{\"") + 2);
                            if (text.IndexOf("[{\"") > 0 && text.IndexOf("}}]") > 0)
                            {
                                string second = "";
                                try
                                {
                                    second = text.Substring(text.IndexOf("[{\""), text.IndexOf("}}]") - text.IndexOf("[{\"") + 3);
                                }
                                catch
                                {
                                    continue;
                                }
                                Newtonsoft.Json.Linq.JObject oFirst = Newtonsoft.Json.Linq.JObject.Parse(first);
                                Newtonsoft.Json.Linq.JArray oSecond = null;
                                try
                                {
                                    oSecond = Newtonsoft.Json.Linq.JArray.Parse(second);
                                }
                                catch
                                {
                                    continue;
                                }
                                Newtonsoft.Json.Linq.JObject oV = Newtonsoft.Json.Linq.JObject.Parse(v);
                                string[] playerVehicle = oFirst["playerVehicle"].ToString().Split(new Char[] { '-' }, 2);
                                int damage = (int)oSecond[0]["damageDealt"];
                                int frags = oSecond[0]["killed"].Count();
                                int spot = oSecond[0]["spotted"].Count();
                                int cap = (int)oSecond[0]["capturePoints"];
                                int def = (int)oSecond[0]["droppedCapturePoints"];

                                int tier;
                                try
                                {
                                    tier = (int)oV[playerVehicle[0]][playerVehicle[1]];
                                }
                                catch
                                {
                                    continue;
                                }

                                
                                int re = (int)(damage * (10 / (tier + 2)) * (0.23 + 2 * tier / 100) + frags * 250 + spot * 150 + Math.Log((double)(cap + 1), (double)1.732) * 150 + def * 150);
                                string mapname = oFirst["mapName"].ToString();
                                string[] nums = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", };
                                if (nums.Contains(mapname.ElementAt<char>(0).ToString()))
                                {
                                    mapname = mapname.Substring(mapname.IndexOf("_") + 1);
                                }
                                try
                                {
                                    oMaps[mapname]["b"] = (int)oMaps[mapname]["b"] + 1;
                                }
                                catch
                                {
                                    continue;
                                }
                                if ((int)oSecond[0]["isWinner"] == 1)
                                {
                                    oMaps[mapname]["w"] = (int)oMaps[mapname]["w"] + 1;
                                }
                                oMaps[mapname]["k"] = (int)oMaps[mapname]["k"] + re;
                                i++;
                                label2.Invoke((MethodInvoker)delegate
                                {
                                    label2.Text = i + "/" + files.Length.ToString();
                                });
                                progressBar1.Invoke((MethodInvoker)delegate
                                {
                                    progressBar1.Value = (int)((double)i / (double)files_count * 100);
                                });
                                int ind = json_indexof(oMaps, mapname);
                                chart1.Invoke((MethodInvoker)delegate
                                {
                                    int cur_re = (int)Math.Round((double)oMaps[mapname]["k"] / (double)oMaps[mapname]["b"], 2);
                                    chart1.Series[0].Points[ind].SetValueY(cur_re);
                                    chart1.ChartAreas[0].RecalculateAxesScale();
                                    System.Drawing.Color color = System.Drawing.Color.FromArgb(254, 14, 0);
                                    if (cur_re >= 0 && cur_re < 630)
                                    {
                                        color = System.Drawing.Color.FromArgb(254, 14, 0);
                                    }
                                    else if (cur_re >= 630 && cur_re < 860)
                                    {
                                        color = System.Drawing.Color.FromArgb(254, 121, 3);
                                    }
                                    else if (cur_re >= 860 && cur_re < 1140)
                                    {
                                        color = System.Drawing.Color.FromArgb(248, 244, 3);
                                    }
                                    else if (cur_re >= 1140 && cur_re < 1460)
                                    {
                                        color = System.Drawing.Color.FromArgb(96, 255, 0);
                                    }
                                    else if (cur_re >= 1460 && cur_re < 1735)
                                    {
                                        color = System.Drawing.Color.FromArgb(2, 201, 179);
                                    }
                                    else if (cur_re >= 1735)
                                    {
                                        color = System.Drawing.Color.FromArgb(208, 66, 243);
                                    }
                                    chart1.Series[0].Points[ind].Color = color;
                                });
                                chart2.Invoke((MethodInvoker)delegate
                                {
                                    double w_p = Math.Round((double)oMaps[mapname]["w"] / (double)oMaps[mapname]["b"] * 100, 2);
                                    chart2.Series[0].Points[ind].SetValueY(w_p);
                                    chart2.ChartAreas[0].RecalculateAxesScale();
                                    System.Drawing.Color color = System.Drawing.Color.FromArgb(254, 14, 0);
                                    if (w_p >= 0 && w_p < 47)
                                    {
                                        color = System.Drawing.Color.FromArgb(254, 14, 0);
                                    }
                                    else if (w_p >= 47 && w_p < 49)
                                    {
                                        color = System.Drawing.Color.FromArgb(254, 121, 3);
                                    }
                                    else if (w_p >= 49 && w_p < 52)
                                    {
                                        color = System.Drawing.Color.FromArgb(248, 244, 3);
                                    }
                                    else if (w_p >= 52 && w_p < 57)
                                    {
                                        color = System.Drawing.Color.FromArgb(96, 255, 0);
                                    }
                                    else if (w_p >= 57 && w_p < 64)
                                    {
                                        color = System.Drawing.Color.FromArgb(2, 201, 179);
                                    }
                                    else if (w_p >= 64)
                                    {
                                        color = System.Drawing.Color.FromArgb(208, 66, 243);
                                    }
                                    chart2.Series[0].Points[ind].Color = color;
                                });
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                    button1.Invoke((MethodInvoker)delegate
                    {
                        button1.Enabled = true;
                    });
                }
                else
                {
                    button1.Invoke((MethodInvoker)delegate
                    {
                        button1.Enabled = true;
                    });
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(worker.IsBusy)
            {
                worker.CancelAsync();
            }
        }

        public int json_indexof(Newtonsoft.Json.Linq.JObject json, string key)
        {
            int num = -1;
            int i = 0;
            foreach (var j in json)
            {
                if(j.Key == key)
                {
                    num = i;
                }
                i++;
            }
            return num;
        }
    }
}